# How to use

- install python3
- install requirments

```
pip3 install requirments.txt
```

- update list of urls in urls.txt
- set env variable with access_tokes

```
export ACCESS_TOKEN="your_token_here"
```

- run the script

```
python3 fb_shares.py
```
