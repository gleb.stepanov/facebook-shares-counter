#!/usr/bin/env python3
import requests
import os

URL = "https://graph.facebook.com/v6.0/"
ACCESS_TOKEN = os.environ['ACCESS_TOKEN']


def main():
    with open('urls.txt') as f:
        lines = [line.rstrip() for line in f]
    print("SHARES", "\t\t", "SITE")
    for site in lines:
        url = "https://graph.facebook.com/v6.0/?id=" + site + \
            "&access_token=" + ACCESS_TOKEN + "\"&fields=engagement"
        payload = {}
        headers = {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
        response = requests.request("GET", url, headers=headers, data=payload)
        site = response.json()["id"]
        share_count = response.json()["engagement"]["share_count"]

        print(share_count, "\t\t", site)


if __name__ == '__main__':
    main()
